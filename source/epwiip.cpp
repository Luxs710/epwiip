#include <cstdio>
#include <cstdlib>
//#include <gccore.h>
#include <wiiuse/wpad.h>
#include <iostream>

#include <mp3player.h> //Audio
#include <asndlib.h>  //Audio

#include "vale_mp3.h"
#include "good_mp3.h"

static void *xfb = NULL;
static GXRModeObj *rmode = NULL;

//---------------------------------------------------------------------------------
int main(int argc, char **argv) {
//---------------------------------------------------------------------------------

	// Initialise the video system
	VIDEO_Init();

	// This function initialises the attached controllers
	WPAD_Init();

    // Initialise the audio subsystem
	ASND_Init();
	MP3Player_Init();


	// Obtain the preferred video mode from the system
	// This will correspond to the settings in the Wii menu
	rmode = VIDEO_GetPreferredMode(NULL);

	// Allocate memory for the display in the uncached region
	xfb = MEM_K0_TO_K1(SYS_AllocateFramebuffer(rmode));

	// Initialise the console, required for printf
	console_init(xfb,20,20,rmode->fbWidth,rmode->xfbHeight,rmode->fbWidth*VI_DISPLAY_PIX_SZ);

	// Set up the video registers with the chosen mode
	VIDEO_Configure(rmode);

	// Tell the video hardware where our display memory is
	VIDEO_SetNextFramebuffer(xfb);

	// Make the display visible
	VIDEO_SetBlack(FALSE);

	// Flush the video register changes to the hardware
	VIDEO_Flush();

	// Wait for Video setup to complete
	VIDEO_WaitVSync();
	if(rmode->viTVMode&VI_NON_INTERLACE) VIDEO_WaitVSync();



	// The console understands VT terminal escape codes
	// This positions the cursor on row 2, column 0
	// we can use variables for this with format codes too
	// e.g. printf ("\x1b[%d;%dH", row, column );

	printf("\x1b[2;0H");


	//printf("Hello World!");
	std::cout<<"\t\tSeja bem vindo ao Ednaldo Pereira Player!\n\n\t\tEscolha alguma das musicas usando\n\t\t as setas do wiimote\n\n";

	std::string lista[3] = {"Para a musica atual","Vale Nada Vale Tudo","God is Good"};
	int pos = 0, ant_pos = 1;
	std::cout<<"<"<<lista[pos]<<">";

	int musica_at = 0; //pega a musica atual



	while(1) {

        ant_pos = pos;
		// Call WPAD_ScanPads each loop, this reads the latest controller states
		WPAD_ScanPads();

		// WPAD_ButtonsDown tells us which buttons were pressed in this loop
		// this is a "one shot" state which will not fire again until the button has been released
		u32 pressed = WPAD_ButtonsDown(0);

		// We return to the launcher application via exit
		if ( pressed & WPAD_BUTTON_HOME ) exit(0);

        if( pressed & WPAD_BUTTON_RIGHT){

          if(pos == 0){ //vai de 0 para 1
            MP3Player_Stop();
            MP3Player_Init();
            pos+=1;
            MP3Player_PlayBuffer(vale_mp3, vale_mp3_size, NULL);
          }
          else if(pos == 1){ // vai de 1 para 2
            MP3Player_Stop();
            MP3Player_Init();
            pos +=1;
            MP3Player_PlayBuffer(good_mp3, good_mp3_size, NULL);
          }
          else if(pos == 2){ // vai de 2 para 0
            pos = 0;
            MP3Player_Stop();
            MP3Player_Init();
          }
        }

        if(ant_pos != pos){
            std::cout << "\033[2J\033[1;1H";
            std::cout<<"\t\tSeja bem vindo ao Ednaldo Pereira Player!\n\n\t\tEscolha alguma das musicas usando\n\t\t as setas do wiimote\n\n";
            std::cout<<"<"<<lista[pos]<<">";
        }


		// Wait for the next frame
		VIDEO_WaitVSync();
	}

	return 0;
}
